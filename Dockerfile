FROM python:3.7
ENV PYTHONUNBUFFERED 1
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app
COPY wait-for-it.sh /wait-for-it.sh
#RUN apk --update --upgrade add gcc musl-dev jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf-dev postgresql postgresql-dev python3-dev


RUN pip install -r requirements.txt
#RUN chmod +x /usr/src/app/entrypoint.sh

COPY . .
#COPY ./entrypoint.sh /
#
#ENTRYPOINT ["sh", "/entrypoint.sh"]