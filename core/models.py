from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import AbstractUser
from django.db import models

from django.db.models import CASCADE
from django.http import Http404
from django.urls import reverse
from django.utils import timezone


class Author(AbstractUser):
    date_birth = models.DateField(verbose_name='День рождения')
    age = models.PositiveSmallIntegerField(default=0)

    def get_absolute_url(self):
        return reverse('author-detail', args=[str(self.id)])

    def set_age(self):
        date_now = timezone.now()
        self.age = relativedelta(date_now.date(), self.date_birth).years

    def save(self, *args, **kwargs):
        if self.date_birth:
            self.set_age()
        return super().save(*args, **kwargs)


class Post(models.Model):
    title = models.CharField(max_length=100)
    author = models.ForeignKey(Author, on_delete=CASCADE, related_name='author', verbose_name='Author',
                               null=True, blank=True)
    text = models.CharField(max_length=1000)
    created = models.DateField(auto_now_add=True)

    class Meta:
        ordering = ('created',)

    def get_absolute_url(self):
        return reverse('core:post-detail', args=[self.id])

    def __str__(self):
        return self.title

    @staticmethod
    def get_this_year_posts():
        this_year = timezone.now().year
        return Post.objects.filter(created__year=this_year)


class Rating(models.Model):
    post = models.ForeignKey(Post, related_name='rating', on_delete=CASCADE)
    count = models.PositiveIntegerField(default=0)
    average_count = models.PositiveIntegerField(default=0)
    average_rating = models.FloatField(default=0)

    def __str__(self):
        return self.post

    def _set_rating(self, val_rating):
        try:
            self.count += 1
            self.average_count += int(val_rating)
            self.average_rating = self.average_count / self.count
        except TypeError:
            raise Http404
