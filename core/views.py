from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views import generic
from django.views.generic import TemplateView

from core.forms import PostCreateForm
from core.models import Post


class MyLoginView(TemplateView):
    template_name = 'login.html'


class PostListView(generic.ListView):
    model = Post
    paginate_by = 3
    template_name = 'list_post.html'
    queryset = model.objects.all()

    def dispatch(self, request, *args, **kwargs):
        # test1
        # commit 1
        # commit 2
        # commit 3
        # commit 4
        return super(PostListView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *, object_list=None, **kwargs):
        return super(PostListView, self).get_context_data()


class PostDetailView(generic.DetailView):
    model = Post

    template_name = 'detail_post.html'
    pk_url_kwarg = 'pk'


class PostCreateView(LoginRequiredMixin, generic.CreateView):
    model = Post
    form_class = PostCreateForm
    template_name = 'create_post.html'

    def form_valid(self, form):
        post = form.save(commit=False)
        post.author = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('core:post-detail', args=(self.object.id,))
