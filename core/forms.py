from django import forms

from core.models import Post


class PostCreateForm(forms.ModelForm):
    CATEGORY_CHOICE = (
        ('', '---------'),
        (1, 'CHOICE 1'),
        (2, 'CHOICE 2'),
        (3, 'CHOICE 3'),
    )
    category = forms.ChoiceField(choices=CATEGORY_CHOICE, required=False, widget=forms.widgets.Select())

    class Meta:
        model = Post
        fields = ('title', 'text', 'category')

    def clean_title(self):
        title = self.cleaned_data['title']
        if Post.objects.filter(title__icontains=title).exists():
            raise forms.ValidationError('Так надо')
        title = title.upper()
        return title
