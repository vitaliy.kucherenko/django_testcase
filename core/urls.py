from django.urls import path

from core import views

app_name = 'core'

urlpatterns = [
    path('login/', views.MyLoginView.as_view(), name='login'),
    path('post-list/', views.PostListView.as_view(), name='post-list'),
    path('post-detail/<int:pk>', views.PostDetailView.as_view(), name='post-detail'),
    path('post-create/', views.PostCreateView.as_view(), name='post-create'),
]
