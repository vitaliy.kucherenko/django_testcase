product = {
    'apple': 10,
    'orange': 10,
    'pear': 10,
}


def check_product_availability(query: str) -> bool:
    return query in product


def operations(a: int, b: int, c: str):
    if c == '+':
        return a + b
    elif c == '-':
        return a - b
    return
