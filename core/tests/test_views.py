import datetime
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from core.models import Post


class PostListViewTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        Post.objects.bulk_create((
            Post(title='1', text='text'),
            Post(title='2', text='text'),
            Post(title='3', text='text'),
            Post(title='4', text='text'),
            Post(title='5', text='text'),
        ))

    def test_post_list_GET(self):
        response = self.client.get(reverse('core:post-list'))
        post_list_expected_value = Post.objects.filter(title__in=(1, 2, 3))
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'list_post.html')
        self.assertTrue(response.context_data['is_paginated'])
        self.assertQuerysetEqual(post_list_expected_value, response.context_data['post_list'], ordered=False)


class PostCreateViewTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = get_user_model().objects.create_user(username='test', password='test',
                                                        date_birth=datetime.datetime(2020, 1, 1))
        cls.url = reverse('core:post-create')

    def test_if_user_not_is_authenticated(self):
        response = self.client.get(self.url)
        self.assertRedirects(response, f"{reverse('core:login')}?next=/post-create/")

    def test_if_user_is_authenticated(self):
        self.client.login(username='test', password='test')
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_create_post(self):
        data = {'title': '1', 'text': '2'}
        self.client.login(username='test', password='test')
        response = self.client.post(self.url, data)
        self.assertEqual(1, Post.objects.count())
        post = Post.objects.get(**data)
        self.assertEqual(self.user, post.author)
        self.assertRedirects(response, reverse('core:post-detail', args=(post.id,)))


class PostDetailViewTestCase(TestCase):
    fixtures = ("author.json", "posts.json")

    @classmethod
    def setUpTestData(cls):
        cls.url = 'core:post-detail'

    def test_method_get(self):
        post = Post.objects.first()
        response = self.client.get(reverse(self.url, args=[post.id]))
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.context_data['post'], post)
