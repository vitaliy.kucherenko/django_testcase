import datetime
from unittest.mock import patch

from django.http import Http404
from django.test import TestCase
from django.urls import reverse

from core.models import Rating, Post, Author


class RatingTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        Post.objects.create(title='post title', text='post text')

    def setUp(self):
        self.rating = Rating.objects.create(post=Post.objects.get(title='post title'))

    def test_set_rating_if_val_rating_is_int(self):
        val_rating = 4
        self.rating._set_rating(val_rating)
        self.assertEqual(1, self.rating.count)
        self.assertEqual(val_rating, self.rating.average_count)
        self.assertEqual(float(val_rating), self.rating.average_rating)

    def test_set_rating_if_val_rating_is_str(self):
        val_rating = '4'
        self.rating._set_rating(val_rating)
        self.assertEqual(1, self.rating.count)
        self.assertEqual(int(val_rating), self.rating.average_count)
        self.assertEqual(float(val_rating), self.rating.average_rating)

    def test_set_rating_if_val_rating_is_not_str_or_int(self):
        val_rating = dict
        with self.assertRaises(Http404):
            self.rating._set_rating(val_rating)


class PostModelTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.model = Post

        cls.model.objects.bulk_create((
            Post(title='post title1', text='post text1'),
            Post(title='post title2', text='post text2'),
            Post(title='post title3', text='post text3'),
        ))

        cls.post1 = cls.model.objects.get(title='post title1', text='post text1')

    def test_get_absolute_url(self):
        actual_value = self.post1.get_absolute_url()
        expected_value = reverse('core:post-detail', args=[self.post1.id])
        self.assertEqual(expected_value, actual_value)

    def test_get_this_year_posts(self):
        posts = self.model.objects.all()
        actual_value = self.model.get_this_year_posts()
        self.assertEqual(3, posts.count())
        # self.assertQuerysetEqual(posts, actual_value, transform=lambda x: x, ordered=False)
        # self.assertCountEqual(posts, actual_value)
        self.assertQuerysetEqual(posts, actual_value)

        old_post = self.model.objects.create(title='old post', text='old')
        old_post.created = datetime.datetime(1995, 1, 1)
        old_post.save()
        posts = self.model.objects.order_by('-title')
        self.assertEqual(4, posts.count())
        self.assertNotIn(old_post.id, posts.values('id'))


class AuthorModelTestCase(TestCase):

    @patch('django.utils.timezone.now', return_value=datetime.datetime(2000, 2, 1))
    def test_save(self, mock_today):
        author = Author.objects.create(username='test', first_name='Dick', last_name='Big',
                                       date_birth=datetime.datetime(1990, 1, 1))
        mock_today.assert_called_once()
        self.assertEqual(10, author.age)

    def test_2_save(self):
        with patch('core.models.Author.set_age') as mock_age:
            author = Author.objects.create(username='test', first_name='Dick', last_name='Big',
                                           date_birth=datetime.datetime(1990, 1, 1))
        mock_age.assert_called_once()
        self.assertEqual(0, author.age)

    @patch.object(Author, 'get_absolute_url')
    def test_get_absolute_url(self, mock_url):
        print(mock_url)
        mock_url.return_value = '123'
        author = Author.objects.create(username='test', first_name='Dick', last_name='Big',
                                       date_birth=datetime.datetime(1990, 1, 1))
        actual = author.get_absolute_url()
        print(actual)
        mock_url.assert_called_once()


class SomeTestCase(TestCase):

    def setUp(self) -> None:
        self.mock_time_now = patch('django.utils.timezone.now', return_value=datetime.datetime(2000, 2, 1))
        self.mock_time_now.start()

    def tearDown(self) -> None:
        self.mock_time_now.stop()

    def test_author_save(self):
        author = Author.objects.create(username='test', first_name='Dick', last_name='Big',
                                       date_birth=datetime.datetime(1990, 1, 1))
        self.assertEqual(10, author.age)
