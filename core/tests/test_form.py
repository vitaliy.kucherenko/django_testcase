import datetime

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.urls import reverse

from core.forms import PostCreateForm
from core.models import Post


class PostCreateFormTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.data = {'title': 'title 2', 'text': 'text 2'}
        Post.objects.create(title='title 1', text='text 1')
        cls.form = PostCreateForm()

    def test_clean_title_if_post_not_have_such_title(self):
        self.form.cleaned_data = self.data
        actual = self.form.clean_title()
        self.assertEqual('TITLE 2', actual)

    def test_clean_title_if_post_have_such_title(self):
        self.data['title'] = 'title 1'
        self.form.cleaned_data = self.data
        with self.assertRaisesMessage(ValidationError, 'Так надо'):
            self.form.clean_title()

    def test_2_clean_title_if_post_have_such_title(self):
        get_user_model().objects.create_user(username='test', password='test', date_birth=datetime.datetime(2020, 1, 1))
        self.client.login(username='test', password='test')
        self.data['title'] = 'title 1'
        response = self.client.post(reverse('core:post-create'), self.data)
        self.assertFormError(response, 'form', 'title', 'Так надо')

    def test_field_category(self):
        choices = (
            ('', '---------'),
            (1, 'CHOICE 1'),
            (2, 'CHOICE 2'),
            (3, 'CHOICE 3'),
        )
        self.assertListEqual(list(choices), self.form.fields['category'].choices)
