from django.test import TestCase

from core.example import check_product_availability, operations


class ExampleTesCase(TestCase):
    data = {'test': 1}

    @classmethod
    def setUpTestData(cls):
        """
        вызывается каждый раз перед запуском теста на уровне настройки всего класса. Вы должны использовать данный
        метод для создания объектов, которые не будут модифицироваться/изменяться в каком-либо из тестовых методов
        """
        print("setUpTestData")

    def setUp(self):
        """
         вызывается перед каждой тестовой функцией для настройки объектов, которые могут изменяться во время тестов
         (каждая функция тестирования будет получать "свежую" версию данных объектов).
        """
        print('setUp')

    def tearDown(self):
        # Очистка после каждого метода
        print('tearDown')

    def test_assert_equal(self):
        expected_value = 1
        actual_value = self.data.get('test')
        self.assertEqual(expected_value, actual_value)

    def test_assert_true(self):
        self.assertTrue(check_product_availability('apple'))

    def test_assert_false(self):
        self.assertFalse(check_product_availability('pineapple'))


class OperationTestCase(TestCase):

    def test_plus(self):
        actual_val = operations(13, 6, '+')
        expected_val = 19
        self.assertEqual(expected_val, actual_val)
    #
    # def test_minus(self):
    #     actual_val = operations(7, 2, '-')
    #     expected_val = 5
    #     self.assertEqual(expected_val, actual_val)
