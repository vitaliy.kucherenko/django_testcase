#!/bin/sh

python3 manage.py wait_for_db
python3 manage.py migrate --no-input
python3 manage.py collectstatic --no-input
python3 manage.py runserver 0.0.0.0:8000
#gunicorn3 django_tescase.wsgi:application --bind 0.0.0.0:8000