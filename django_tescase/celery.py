import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_tescase.settings')

app = Celery('django_tescase')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
