CREATE USER super_user WITH PASSWORD 'devpass';
ALTER USER super_user CREATEDB;
CREATE DATABASE prod_db;
GRANT ALL PRIVILEGES ON DATABASE prod_db TO super_user;
